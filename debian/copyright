Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/MatthiasValvekens/pyHanko
Upstream-Name: pyhanko
Upstream-Contact: https://github.com/MatthiasValvekens/pyHanko/issues
Files-Excluded: pyhanko_tests/data/fonts/*
                CONTRIBUTING.md
Comment: The included fonts are available in Debian.
         The contributing guideline has a non-free document included.

Files: *
Copyright: (c) 2020-2023 Matthias Valvekens
License: Expat

Files: external-schemata/xsd/w3c/xmldsig_core.xsd
Copyright: Copyright 2001 The Internet Society and W3C (Massachusetts Institute
 of Technology, Institut National de Recherche en Informatique et en
 Automatique, Keio University). All Rights Reserved.
 http://www.w3.org/Consortium/Legal/
Comment:
 This document is governed by the W3C Software License [1] as described
 in the FAQ [2].
 .
 [1] http://www.w3.org/Consortium/Legal/copyright-software-19980720
 [2] http://www.w3.org/Consortium/Legal/IPR-FAQ-20000620.html#DTD
License: W3C-20150513
 This W3C work (including software, documents, or other related items) is being
 provided by the copyright holders under the following license. By obtaining,
 using and/or copying this work, you (the licensee) agree that you have read,
 understood, and will comply with the following terms and conditions:
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation, with or without modification,  for any purpose and without fee
 or royalty is hereby granted, provided that you include the following on ALL
 copies of the software and documentation or portions thereof, including
 modifications, that you make:
 .
 1. The full text of this NOTICE in a location viewable to users of the
    redistributed or derivative work.
 2. Any pre-existing intellectual property disclaimers, notices, or terms and
    conditions. If none exist, a short notice of the following form (hypertext
    is preferred, text is permitted) should be used within the body of any
    redistributed or derivative code: "Copyright © [$date-of-software] World Wide
    Web Consortium, (Massachusetts Institute of Technology, Institut National de
    Recherche en Informatique et en Automatique, Keio University).
    All Rights Reserved. http://www.w3.org/Consortium/Legal/"
 3. Notice of any changes or modifications to the W3C files, including the date
    changes were made. (We recommend you provide URIs to the location from which
    the code is derived.)
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE
 NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT
 THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY
 PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR DOCUMENTATION.
 .
 The name and trademarks of copyright holders may NOT be used in advertising or
 publicity pertaining to the software without specific, written prior
 permission. Title to copyright in this software and any associated
 documentation will at all times remain with copyright holders.

Files: pyhanko/pdf_utils/*
Copyright: (c) 2006-2008, Mathieu Fenniak
 Some contributions copyright (c) 2007, Ashish Kulkarni <kulkarni.ashish@gmail.com>
 Some contributions copyright (c) 2014, Steve Witham <switham_github@mac-guyver.com>
 .
 All rights reserved.
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * The name of the author may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: pyhanko/pdf_utils/crypt/_saslprep.py
Copyright: 2016-present MongoDB, Inc.
 Some changes copyright 2021-present Matthias Valvekens,
 licensed under the license of the pyHanko project.
License: Apache-2 and Expat

Files: debian/*
Copyright: (c) 2024 Bastian Germann
License: Expat

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
Comment:
 On Debian systems, the full text of the License can be found in the file
 `/usr/share/common-licenses/Apache-2.0'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
